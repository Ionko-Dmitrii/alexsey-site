from django.views.generic import TemplateView


class IndexView(TemplateView):
    """Представление для главной страницы"""
    template_name = 'pages/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        return context
