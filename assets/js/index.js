let portfolioCard = $('.js-HoverCard');
let bodySelector = $('body');
let mainSection = $('.js-hideBackground');
let logoColor = $('.logo .icon');
let header = $('.header');
let timeOut = null;

portfolioCard.on("mouseover", function () {
    let background = $(this).data('background');
    if(timeOut) clearTimeout(timeOut);
    bodySelector.css('background-color', background)
})

portfolioCard.on("mouseout", function () {
    bodySelector.css('background-color', '#000000');
})

$(document).on('scroll', function(){
    if($(this).scrollTop() > 50){
        mainSection.css({'background-color': 'transparent', 'color': '#ffffff'})
        logoColor.css('fill', '#ffffff')
        header.css('color', '#ffffff')
    } else {
        mainSection.css({'background-color': '#ffffff', 'color': '#000000'})
        logoColor.css('fill', '#000000')
        header.css('color', '#000000')
    }
})
